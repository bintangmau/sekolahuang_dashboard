import React, { useState } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';

// PAGES
import SignIn from './Pages/SignIn';
import Main from './Pages/Main';
import Classes from './Pages/Classes';
import Articles from './Pages/Articles';
import CreateArticle from './Pages/Articles/CreateArticle';
import CreateClass from './Pages/Classes/CreateClass';
import EditClass from './Pages/Classes/EditClass';
import Assumption from './Pages/Assumption';
import CreateAssumption from './Pages/Assumption/CreateAssumption';

// COMPONENTS
import Navbar from './Components/Navbar';
import Drawer from './Components/Drawer';

function App() {
	const [open, setOpen] = useState(false);
	const handleDrawerOpen = () => {
	setOpen(true);
	};
	const handleDrawerClose = () => {
		setOpen(false);
	};
	return (
		<div className="App">
			<Navbar open={open} handleDrawerOpen={handleDrawerOpen} />
			<Drawer open={open} handleDrawerClose={handleDrawerClose} />
			<Switch>
				<Route exact path='/' component={Main} />
				<Route path='/login' component={SignIn} />
        		<Route path='/articles' component={Articles} />
        		<Route path='/articles-create' component={CreateArticle} />
				<Route path='/classes' component={Classes}/>
        		<Route path='/classes-create' component={CreateClass} />
        		<Route path='/classes-edit/:uuid' component={EditClass} />
        		<Route path='/assumption' component={Assumption} />
        		<Route path='/assumption-create' component={CreateAssumption} />
			</Switch>
			
		</div>
	);
}

export default withRouter(App);
