import { configureStore } from '@reduxjs/toolkit'
import counterReducer from './Reducers/counter';

export default configureStore({
  reducer: {
    counter: counterReducer
  }
});