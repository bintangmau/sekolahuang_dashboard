import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Typography
} from '@material-ui/core';
import Cookies from 'universal-cookie';
import { Redirect } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  content: {
    flexGrow: 1,  
    height: 600,
    width: "100%",
    overflow: 'auto',
    paddingTop: "80px",
    paddingLeft: "50px",
    paddingRight: "50px",
    backgroundColor: "white"
  },
  headerContent: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: "10px"
  },
}));

export default function Main() {
  const classes = useStyles();
  const cookies = new Cookies();
  const token = cookies.get('auth');

  if(!token) return <Redirect to='/login'/>

  return (
    <main className={classes.content}>
      <Typography 
        variant="h6" 
        gutterBottom
      >
        Dashboard Money University 
      </Typography>
    </main>
  )
}