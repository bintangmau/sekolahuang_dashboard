import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Typography,
  Grid,
  Button
} from '@material-ui/core';
import Cookies from 'universal-cookie';
import swal from 'sweetalert';
import validateToken from '../../../Common/validateToken';
import axios from 'axios';
import baseUrl from '../../../Common/apiurl';

import Loader from '../../../Components/Loader';

const useStyles = makeStyles((theme) => ({
  content: {
    flexGrow: 1,
    width: "90%",
    overflow: 'auto',
    paddingTop: "80px",
    paddingLeft: "5%",
    paddingRight: "5%",
    backgroundColor: "white"
  },
  mainContent: {
    marginTop: '40px'
  },
  inputBox: {
    marginBottom: '20px'
  },
  inputTitle: {
    fontSize: '14px'
  },
  input: {
    height: '30px',
    width: '20%',
    padding: '5px',
    outline: 'none',
    paddingLeft: '7px'
  },
  inputTextarea: {
    padding: '5px',
    paddingLeft: '7px'
  },
  buttonBox: {
    marginBottom: '100px',
    marginTop: '60px'
  },
  saveButton: {
    backgroundColor: "black",
  }
}));

export default function EditArticle({ match }) {
  const classes = useStyles();
  const cookies = new Cookies();
  const token = cookies.get('auth');
  const [isLoggedIn, setIsLoggedIn] = useState(true);
  const [title, setTitle] = useState('');
  const [subTitle, setSubTitle] = useState('');
  const [text, setText] = useState('');
  const [thumbnail, setThumbnail] = useState(null);
  const [source, setSource] = useState('');
  const [isCreating, setIsCreating] = useState(false);

  const saveArticle = () => {
    setIsCreating(true);
    let formData = new FormData();
    let headers  = {
      'Content-Type': 'multipart/form-data',
      Authorization: `Bearer ${token}`
    };
    let data = {
      title,
      subTitle,
      text,
      source
    }
    formData.append('data', JSON.stringify(data));
    formData.append(`file_request`, thumbnail);

    axios({
      method: "POST",
      url: baseUrl + 'news/create',
      headers,
      data: formData
    })
    .then((res) => {
      swal('Success', 'Create article success');
      setIsCreating(false);
      return;
    })
    .catch((err) => {
      swal('Error', 'Create article failed');
      setIsCreating(false);
      return;
    })
    .finally(() => {
      setTitle('');
      setSubTitle('');
      setText('');
      setThumbnail(null);
      setSource('');
    })
  }

  useEffect(() => {
    async function getValidate() {
      let isValidated = await validateToken();
      if(!isValidated) {
        setIsLoggedIn(false);
      }   
    }
    getValidate()
  }, [])

  // if(!isLoggedIn) return <Redirect to='/login'/>

  return (
    <main className={classes.content}>
      <Typography 
        variant="h6" 
        gutterBottom
      >
        Create Articles {match.params.uuid}
      </Typography>

      <div>

        <div className={classes.inputBox}>
          <p className={classes.inputTitle}>Title</p>
          <input 
            onChange={(e) => setTitle(e.target.value)}
            value={title}
            className={classes.input}
            placeholder="Article Title"
            type="text"
          />
        </div>

        <div className={classes.inputBox}>
          <p className={classes.inputTitle}>Sub Title</p>
          <input 
            onChange={(e) => setSubTitle(e.target.value)}
            value={subTitle}
            className={classes.input}
            placeholder="Article Sub-Title"
            type="text"
          />
        </div>

        <div className={classes.inputBox}>
          <p className={classes.inputTitle}>Text</p>
          <textarea 
            onChange={(e) => setText(e.target.value)}
            value={text}
            className={classes.inputTextarea}
            cols="50" 
            rows="20"
            placeholder="Article Text"
          />
        </div>

        <div>
          <p className={classes.inputTitle}>Thumbnail</p>
          <input 
            onChange={(e) => setThumbnail(e.target.files[0])}
            className={classes.input}
            type="file" 
          />
        </div>

        <div className={classes.inputBox}>
          <p className={classes.inputTitle}>Source</p>
          <input 
            value={source}
            onChange={(e) => setSource(e.target.value)}
            className={classes.input}
            placeholder="Article Source"
            type="text"
          />
        </div>

        <div className={classes.buttonBox}>
          <Button
            onClick={saveArticle}
            className={classes.saveButton}
            variant="contained"
            color="primary"
          >
          {
            isCreating
            ?
            <center>
              <Loader />
            </center>
            :
            <>
              {`Save Article`}
            </>
          }
          </Button>
        </div>

      </div>
    </main>
  )
}