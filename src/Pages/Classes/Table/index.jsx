import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Loader from '../../../Components/Loader';
import {
  Button
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';
import axios from 'axios';
import baseUrl from '../../../Common/apiurl';

const useStyles = makeStyles((theme) => ({
  buttonBox: {
    display: "flex",
    justifyContent: "space-between"
  },
  link: {
    textDecoration: 'none'
  }
}));

export default function ClassTable() {
  const classes = useStyles();
  const cookies = new Cookies();
  const token = cookies.get('auth');
  const [classData, setClassData] = useState([]);
  const [isGettingData, setIsGettingData] = useState(false);

  const getClassData = () => {
    setIsGettingData(true);
    axios({
      method: "GET",
      url: baseUrl + 'class/get/by-creator',
      headers: { 
        Authorization: `Bearer ${token}`
      },
    })
    .then((res) => {
      setClassData(res.data.data);
    })
    .catch((err) => {

    })
    .finally(() => {
      setIsGettingData(false);
    })
  }

  useEffect(() => {
    getClassData()
  }, [])

  if(isGettingData) return <Loader />

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Description</TableCell>
            <TableCell align="right">Level</TableCell>
            <TableCell align="right">Created At</TableCell>
            <TableCell align="right">Last Update</TableCell>
            <TableCell align="right"></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {classData.map((row) => (
            <TableRow
              key={row.id}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell>{row.name}</TableCell>
              <TableCell 
                component="th" 
                scope="row" 
              >
                {row.description}
              </TableCell>
              <TableCell align="right">{row.level}</TableCell>
              <TableCell align="right">{row.createdAt}</TableCell>
              <TableCell align="right">{row.updatedAt}</TableCell>
              <TableCell align="right">
                <div className={classes.buttonBox}>
                  <Link to={`/classes-edit/${row.uuid}`} className={classes.link}>
                    <Button
                      variant="contained"
                      color="primary"
                      size="small"
                    >
                      Edit
                    </Button>
                  </Link>

                  <Link to='/classes-delete' className={classes.link}>
                    <Button
                      variant="contained"
                      color="secondary"
                      size="small"
                    >
                      Delete
                    </Button>
                  </Link>
                </div>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
