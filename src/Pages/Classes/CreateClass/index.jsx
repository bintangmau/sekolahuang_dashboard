import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Typography,
  Grid,
  Button
} from '@material-ui/core';
import { Redirect, Link } from 'react-router-dom';

import Create from './create';
import validateToken from '../../../Common/validateToken';

const useStyles = makeStyles((theme) => ({
  content: {
    flexGrow: 1,  
    width: "95%",
    overflow: 'auto',
    paddingTop: "80px",
    paddingLeft: "25px",
    paddingRight: "25px",
    paddingBottom: "50px",
    backgroundColor: "white"
  },
  headerContent: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: "10px"
  }
}));

export default function CreateClass() {
  const classes = useStyles();
  const [isLoggedIn, setIsLoggedIn] = useState(true);

  useEffect(() => {
    async function getValidate() {
      let isValidated = await validateToken();
      if(!isValidated) {
        setIsLoggedIn(false);
      }   
    }
    getValidate()
  }, [])

  if(!isLoggedIn) return <Redirect to='/login'/>
  
  return (
    <main className={classes.content}>


      <Grid className={classes.headerContent}>
        <Typography 
          variant="h6" 
          gutterBottom
        >
          Create Class 
        </Typography>
        <Link to='/classes' className={classes.showLinkBtg}>
          <Button
            variant="contained"
            color="primary"
          >
            Show
          </Button>
        </Link>
      </Grid>

      <Create />

    </main>
  )
}