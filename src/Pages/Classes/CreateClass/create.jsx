import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Typography,
  Grid,
  Button,
  TextField
} from '@material-ui/core';
import axios from 'axios';
import baseUrl from '../../../Common/apiurl';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';
import { Redirect } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  mainContent: {
    marginTop: "20px",
    backgroundColor: '#F1F1F1',
    padding: '20px',
  },
  input: {
    textDecoration: 'none',
    outline: 'none',
    height: '30px',
    width: '25%',
    padding: '5px'
  },
  textAreaBox: {
    marginTop: '20px'
  },
  textArea: {
    width: '25%',
    height: '100px',
    padding: '5px'
  },
  button: {
    marginTop: '20px'
  }
}));

export default function Create() {
  const classes = useStyles();
  const cookies = new Cookies();
  const token = cookies.get('auth');
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [level, setLevel] = useState('Beginner');
  const [isClassCreated, setIsClassCreated] = useState(false);
  const [isFailedGetByUUID, setIsFailedGetByUUID] = useState(false);
  const [classData, setClassData] = useState(null);

  const addClass = () => {
    if(!name) {
      swal('Warning!', 'Name is empty');
      return;
    }
    if(!description) {
      swal('Warning', 'Description is empty');
      return;
    }
    axios({
      method: "POST",
      url: baseUrl + 'class/create',
      data: {
        name, 
        description,
        level
      },
      headers: { 
        Authorization: `Bearer ${token}`
      },
    })
    .then((res) => {
      setName('');
      setDescription('');
      swal('Success', 'Create success');
      setIsClassCreated(true);
      getByUUID(res.data.data.name);
    })
    .catch((err) => {
      swal('Error', 'Create failed');
    });
  } 

  const getByUUID = (name) => {
    axios({
      method: "GET",
      url: baseUrl + `class/get/by-uuid?uuid=${name}`,
      headers: { 
        Authorization: `Bearer ${token}`
      },
    })
    .then((res) => {
      setClassData(res.data.data);
      return;
    })
    .catch((err) => {
      setIsFailedGetByUUID(true);
      return;
    })
  }

  if(isFailedGetByUUID) return <Redirect to='/classes'/>

  if(classData) {
    return (
      <div className={classes.mainContent}>
        <h4>
          {classData.name}
        </h4>
      </div>
    )
  }
  return (
    <div className={classes.mainContent}>

      <div>
        <input 
          onChange={(e) => setName(e.target.value)}
          value={name}
          className={classes.input}
          type="text"
          placeholder="Class Name"
        />
      </div>

      <div className={classes.textAreaBox}>
        <textarea 
          onChange={(e) => setDescription(e.target.value)}
          value={description}
          className={classes.textArea}
          placeholder="Description"
        />
      </div>

      <div>
        <Button
          onClick={addClass}
          className={classes.button}
          variant="contained"
          color="primary"
        >
          Add Class
        </Button>
      </div>
    </div>
  )
}