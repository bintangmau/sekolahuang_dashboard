import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Grid,
  Typography,
  Button
} from '@material-ui/core';
import { Redirect } from 'react-router-dom';
import Cookies from 'universal-cookie';
import axios from 'axios';
import baseUrl from '../../../Common/apiurl';
import validateToken from '../../../Common/validateToken';
import swal from 'sweetalert';

// COMPONENTS
import CreateVideo from './CreateVideo';
import CreateModule from './CreateModule';

const useStyles = makeStyles((theme) => ({
  content: {
    flexGrow: 1,  
    width: "95%",
    overflow: 'auto',
    paddingTop: "80px",
    paddingLeft: "25px",
    paddingRight: "25px",
    paddingBottom: "50px",
    backgroundColor: "white"
  },
  headerContent: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: "10px",
    // width: '20%'
  },
  editBox: {
    display: 'flex'
  },
  editInput: {
    textDecoration: 'none',
    outline: "none",
    padding: '5px',
    height: '18px',
    marginTop: '15px',
    marginLeft: '10px'
  },
  saveButton: {
    marginRight: '15px'
  }
}));

export default function EditClass({ match }) {
  const classes = useStyles();
  const cookies = new Cookies();
  const token = cookies.get('auth');
  const [isLoggedIn, setIsLoggedIn] = useState(true);
  const [className, setClassName] = useState('');
  const [classDesciption, setClassDescription] = useState('');
  const [classLevel, setClassLevel] = useState('');
  const [isEdit, setIsEdit] = useState(false);

  const getClassDetail = () => {
    axios({
      method: "GET",
      url: baseUrl + `class/get/detail?uuid=${match.params.uuid}`,
      headers: { 
        Authorization: `Bearer ${token}`
      }
    })
    .then((res) => {
      setClassName(res.data.data.name);
      setClassDescription(res.data.data.description);
      setClassLevel(res.data.data.level);
    })
    .catch((err) => {
      console.log(err, "ERROR")
    })
  }

  const editClass = () => {
    axios({
      method: "PUT",
      url: baseUrl + `class/edit?uuid=${match.params.uuid}`,
      data: {
        name: className,
        description: classDesciption,
        level: classLevel
      },
      headers: { 
        Authorization: `Bearer ${token}`
      }
    })
    .then((res) =>  {
      swal('Success', "Edit Class success");
      setIsEdit(false);
    })
    .catch((err) => {
      swal('Error', "Edit Class failed");
    })
  }

  useEffect(() => {    
    async function getValidate() {
      let isValidated = await validateToken();
      if(!isValidated) {
        setIsLoggedIn(false);
      }   
    }
    getValidate();
    getClassDetail();
  }, [])
  
  if(!isLoggedIn) return <Redirect to='/login'/>

  return (
    <main className={classes.content}>
      <Grid className={classes.headerContent}>
        <Typography 
          variant="h6" 
          gutterBottom
        >
          Edit Class 
        </Typography>
        {
          isEdit 
          ?
          <Grid>
            <Button
              onClick={editClass}
              className={classes.saveButton}
              variant="contained"
              color="primary"
              size="small"
            >
              Save
            </Button> 
            <Button
              onClick={() => setIsEdit(false)}
              variant="contained"
              color="secondary"
              size="small"
            >
              Cancel
            </Button>
          </Grid>
          :
          <Grid>
            <Button
              onClick={() => setIsEdit(true)}
              variant="contained"
              color="primary"
              size="small"
            >
              Edit
            </Button>
          </Grid>
        }
      </Grid>

      <Grid>
        {
          isEdit
          ?
          <>
            <div className={classes.editBox}> 
              <h4>Class Name: </h4>
              <input 
                onChange={(e) => setClassName(e.target.value)}
                value={className}
                type="text"
                className={classes.editInput} 
              />
            </div>
            <div className={classes.editBox}> 
              <h4>Class Description: </h4>
              <input 
                onChange={(e) => setClassDescription(e.target.value)}
                value={classDesciption}
                type="text"
                className={classes.editInput}
              />
            </div>
          </>
          :
          <>
            <h4>Class Name: {className}</h4>
            <h5>Class Description: {classDesciption}</h5>
          </>
        }
      </Grid>

      <CreateVideo />

      <CreateModule />

    </main>
  )
}