import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Typography,
  Grid,
  Button,
  TextField
} from '@material-ui/core';
import axios from 'axios';
import baseUrl from '../../../../Common/apiurl';

const useStyles = makeStyles((theme) => ({
  mainContent: {
    marginTop: "20px",
    backgroundColor: '#F1F1F1',
    padding: '20px'
  },
  videoInputBox:{
    marginTop: '40px'
  },
  videoInput: {
    marginRight: '15px'
  },
  videoValuesInput: {
    marginLeft: '20px'
  },
  videoAdd: {
    marginTop: '40px'
  },
  videoDel: {
    marginLeft: '10px'
  },
  categoryNameSpace: {
    marginTop: '20px'
  },
  changeVideoBtn: {
    marginLeft: '10px'
  },
  inputEditCategory: {
    width: '20%',
    height: '30px',
    textDecoration: 'none',
    outline: 'none',
    marginLeft: 'auto',
    marginRight: 'auto',
    fontWeight: 500
  },
}));

export default function CreateVideo() {
  const classes = useStyles();
  const [isEditCategory, setIsEditCategory] = useState(false);
  const [videos, setVideos] = useState([]);
  const [videoCategoryName, setVideoCategoryName] = useState('Introduction');
  const [inputsVideo, setInputsVideo] = useState([{ type: 'video' }]);


  const renderInputVideo = () => {
    return inputsVideo.map((val, idx) => {
      return (
        <div
          className={classes.videoInputBox}
        >

          <input 
            onChange={changeVideo} 
            className={classes.videoInput}
            type="file" 
          />
          {
            inputsVideo.length - idx === 1
            ?
            <>
            <Button
              onClick={addInputsVideo}
              variant="contained"
              color="primary"
              size='small'
            >
              +
            </Button>
            <Button
              onClick={() => delInputsVideo(idx)}
              className={classes.videoDel}
              variant="contained"
              color="secondary"
              size='small'
            >
              -
            </Button>
            </>
            :
            null
          }
        </div>
      )
    }) 
  }
  
  const addInputsVideo = () => {
    if(inputsVideo.length - videos.length > 0) {
      alert('video is empty')
      return;
    }
    let currentInputs = [...inputsVideo];
    currentInputs.push({ type: 'video' });
    setInputsVideo(currentInputs);
    return;
  }

  const delInputsVideo = (idx) => {
    let videoArr = [...videos];
    let inputsArr = [...inputsVideo];
    videoArr.splice(idx, 1);
    inputsArr.splice(idx, 1);
    setVideos(videoArr);
    setInputsVideo(inputsArr);
  }
  
  const changeVideo = (e) => {
    let currentVideos = [...videos];
    currentVideos.push(e.target.files[0]);
    setVideos(currentVideos);
    return;
  };

  const addVideos = async () => {
    let formData = new FormData();
    let options = { 
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    };
    let data = {
      category: videoCategoryName
    };
    formData.append('data', JSON.stringify(data));

    for(var i = 0; i < videos.length; i++) {
      formData.append(`${videos[i].name}`, videos[i]);
    }
    
    // axios({
    //   method: "POST",
    //   url: `${baseUrl}/videos`
    // })
  }

  return (
    <Grid className={classes.mainContent}>
      <div>
        <Typography 
          variant="p" 
          gutterBottom
          >
          Video
        </Typography>

        <div 
          className={classes.categoryNameSpace}
        />

        {renderInputVideo()}

        <Button
          onClick={addVideos}
          className={classes.videoAdd}
          variant="contained"
          color="primary"
        >
          Save video
        </Button>
      </div>
    </Grid>
  )

}