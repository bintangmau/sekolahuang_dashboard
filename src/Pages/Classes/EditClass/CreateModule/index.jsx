import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Typography,
  Button
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  mainContent: {
    marginTop: "20px",
    backgroundColor: '#F1F1F1',
    padding: '20px'
  },
  moduleBox: {
    marginTop: '40px'
  },
  moduleSave: {
    marginTop: '40px'
  }
}));

export default function CreateModule() {
  const classes = useStyles();

  return (
    <div className={classes.mainContent}>
      <Typography 
        variant="p" 
        gutterBottom
      >
        Module
      </Typography>

      <div className={classes.moduleBox}>
        <input 
          type="file" 
          name="Upload Module" 
          id="uploadModule"
        />
      </div>

      <Button
        // onClick={addVideos}
        className={classes.moduleSave}
        variant="contained"
        color="primary"
      >
        Save Module
      </Button>

    </div>
  )
}