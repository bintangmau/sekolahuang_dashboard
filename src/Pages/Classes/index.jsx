import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Typography,
  Grid,
  Button
} from '@material-ui/core';
import { Link, Redirect } from 'react-router-dom';
import Table from './Table';
import validateToken from '../../Common/validateToken';

const useStyles = makeStyles((theme) => ({
  content: {
    flexGrow: 1,
    height: 600,
    width: "90%",
    overflow: 'auto',
    paddingTop: "80px",
    paddingLeft: "5%",
    paddingRight: "5%",
    backgroundColor: "white"
  },
  headerContent: {
    display: "flex",
    width: "100&",
    justifyContent: "space-between",
    marginBottom: "10px"
  },
  mainContent: {
    marginTop: '40px'
  },
  createLinkBtn: {
    textDecoration: 'none'
  }
}));

export default function Classes() {
  const classes = useStyles();
  const [isLoggedIn, setIsLoggedIn] = useState(true);

  useEffect(() => {
    async function getValidate() {
      let isValidated = await validateToken();
      if(!isValidated) {
        setIsLoggedIn(false);
      }   
    }
    getValidate()
  }, [])

  if(!isLoggedIn) return <Redirect to='/login'/>

  return(
    <main className={classes.content}>
      <Grid className={classes.headerContent}>
        <Typography 
          variant="h6" 
          gutterBottom
        >
          Classes 
        </Typography>
        <Link to='/classes-create' className={classes.createLinkBtn}>
          <Button
            variant="contained"
            color="primary"
            >
            Create
          </Button>
        </Link>
      </Grid>

      <Grid className={classes.mainContent}>
        <Table />
      </Grid>
    </main>
  )
}