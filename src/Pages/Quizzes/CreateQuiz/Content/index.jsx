// import React from 'react';
// import { makeStyles } from '@material-ui/core/styles';
// import { useSelector, useDispatch } from 'react-redux';
// import {
//   decrement,
//   increment
// } from '../../../Redux/Reducers/counter';

// const useStyles = makeStyles((theme) => ({
//   content: {
//     flexGrow: 1,
//     height: '100vh',
//     overflow: 'auto',
//     paddingTop: "50px",
//     paddingLeft: "20px",
//     paddingRight: "20px"
//   },
// }));

// export default function CreateQuiz() {
//   const classes = useStyles();
//   const count = useSelector(state => state.counter.value);
//   const countol = useSelector(state => state.counter.kontol);
//   const dispatch = useDispatch();
  
//   return (
//     <main className={classes.content}>
//       <h1>Create Quiz</h1>
//       <div>
//         <button
//           aria-label="Increment value"
//           onClick={() => dispatch(increment())}
//         >
//           Increment
//         </button>
//         <span>{countol}</span>
//         <button
//           aria-label="Decrement value"
//           onClick={() => dispatch(decrement())}
//         >
//           Decrement
//         </button>
//       </div> 
//     </main>
//   )
// }
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

// MODULES
import baseUrl from '../../../../Common/apiurl';
import swal from 'sweetalert';
import axios from 'axios';
import Cookie from 'universal-cookie';


const useStyles = makeStyles((theme) => ({
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
    paddingTop: "15px",
  },
  uploadText: {
    marginLeft: "10px"
  },
  menuItem: {
    fontSize: '14px'
  },
  formControl: {
    minWidth: 200,
  },
  title: {
    marginBottom: "30px"
  },
  uploadBtn: {
    float: "right"
  },
  uploadBox: {
    marginBottom: "50px"
  }
}));

export default function Content(props) {
  const {
    setIsOpen
  } = props;
  const classes = useStyles();
  const [ quiz, setQuiz ] = useState(null);
  const [ category, setCategory ] = useState('');
  const [ quizName, setQuizName ] = useState('');
  const cookie = new Cookie();
  const token = cookie.get('auth');

  const handleUploadFile = (e) => {
    setQuiz(e.target.files[0])
  };

  const handleChangeCategory = (event) => {
    setCategory(event.target.value);
  };

  const upload = () => {
    if(!quiz) {
      swal("Warning!", "Upload quiz file")
    } else if(!category) {
      swal("Warning!", "Choose quiz category")
    } else if(!quizName) {
      swal("Warning!", "Fill quiz name")
    } else {
      const formData = new FormData();
      formData.append('file', quiz);
      axios({
        method: "POST",
        url: `${baseUrl}quiz/upload`,
        headers: { 
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${token}`
        },
        data: formData,
        params: {
          name: quizName,
          category: category
        }
      })
      .then((res) => {
        console.log(res.data, "BISA ANYING")
        swal("Success", "Upload quiz success");
        setIsOpen(false)
      })
      .catch((err) => {
        console.log(err.response, "ERROR ASUU")
        swal("Error", "Upload failed");
      })
    }
  }

  return (
    <main className={classes.content}>
      <Grid container spacing={3}>
        <Grid item xs={3} sm={6}>
          <Button
            variant="contained"
            component="label"
          >
          {
            quiz
            ?
              <>Change File</>
            :
              <>Upload File</>
          }
          <input
            type="file"
            hidden
            onChange={handleUploadFile}
          />
          </Button>
          <Typography 
            variant="caption" 
            gutterBottom
            className={classes.uploadText}
          >
            {
              quiz
              ?
              <>{`File Selected`}</>
              :
              <>{`No file choosen`}</>           
            }
          </Typography>
        </Grid>

        <Grid item xs={12} sm={6}>
          <FormControl variant="outlined" className={classes.formControl}>
            <InputLabel className={classes.selectLabel} id="category-select">Category</InputLabel>
            <Select
              labelId="category-select"
              value={category}
              onChange={handleChangeCategory}
              label="Category"
              labelWidth={10}
              className={classes.selectEmpty}
            >
              <MenuItem className={classes.menuItem} value={"choice"}>Choice</MenuItem>
              <MenuItem className={classes.menuItem} value={"essay"}>Essay</MenuItem>
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={12}>
          <TextField
            required
            id="quizname"
            name="quizname"
            label="Quiz Name"
            fullWidth
            autoComplete="shipping quizname"
            onChange={(e) => setQuizName(e.target.value)}
            value={quizName}
          />
        </Grid>


        <Grid item xs={12}>
          <TextField
            id="address2"
            name="address2"
            label="Address line 2"
            fullWidth
            autoComplete="shipping address-line2"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="city"
            name="city"
            label="City"
            fullWidth
            autoComplete="shipping address-level2"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField id="state" name="state" label="State/Province/Region" fullWidth />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="zip"
            name="zip"
            label="Zip / Postal code"
            fullWidth
            autoComplete="shipping postal-code"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="country"
            name="country"
            label="Country"
            fullWidth
            autoComplete="shipping country"
          />
        </Grid>

        <Grid item xs={12}>
          <FormControlLabel
            control={<Checkbox color="primary" name="saveAddress" value="yes" />}
            label="Use this address for payment details"
          />
        </Grid>
        
        <Grid item xs={12} sm={6}>
        </Grid>
        <Grid 
          item 
          xs={12} 
          sm={6}
          className={classes.uploadBox}
        >
          <Button
            variant="contained"
            color="primary"
            component="label"
            className={classes.uploadBtn}
            onClick={upload}
          >
            Upload
          </Button>
        </Grid>

      </Grid>
    </main>
  );
}