import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { DataGrid } from '@material-ui/data-grid';
import Typography from '@material-ui/core/Typography';
import CreateQuiz from './CreateQuiz';
import axios from 'axios';
import baseUrl from '../../Common/apiurl';
import Cookie from 'universal-cookie';

const useStyles = makeStyles((theme) => ({
  content: {
    flexGrow: 1,
    height: 600,
    width: "100%",
    overflow: 'auto',
    paddingTop: "80px",
    paddingLeft: "50px",
    paddingRight: "50px",
    backgroundColor: "white"
  },
  headerContent: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: "10px"
  }
}));

const columns = [
  { field: 'id', headerName: 'ID', width: 100 },
  { field: 'name', headerName: 'Quiz Name', width: 200 },
  { field: 'path', headerName: 'Path', width: 320 },
  { field: 'filename', headerName: 'File Name', width: 320 }
]

export default function DataTable() {
  const classes = useStyles();
  const [ dataQuiz, setDataQuiz ] = useState([]);

  useEffect(() => {
    const cookie = new Cookie();
    const token = cookie.get('auth');
    const getListQuiz = () => {
      axios({
        method: "GET",
        url: `${baseUrl}quiz/names`,
        headers: { 
          Authorization: `Bearer ${token}`
        },
      })
      .then((res) => {
        setDataQuiz(res.data.data)
      })
      .catch((err) => {
        console.log(err.response, "ERROR")
      })
    }
    getListQuiz()
  }, [])

  return (
    <main className={classes.content}>
      <Grid className={classes.headerContent}>
        <Typography 
          variant="h6" 
          gutterBottom
          >
          Quizzes 
        </Typography>
        <CreateQuiz />
      </Grid>
      <DataGrid 
        rows={dataQuiz} 
        columns={columns} 
        pageSize={7} 
      />
    </main>
  );
}