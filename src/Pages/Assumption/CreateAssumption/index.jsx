import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Typography,
  Grid,
  Button
} from '@material-ui/core';
import { Link, Redirect } from 'react-router-dom';
import validateToken from '../../../Common/validateToken';
import Cookies from 'universal-cookie';
import swal from 'sweetalert';
import axios from 'axios';
import baseUrl from '../../../Common/apiurl';

import Loader from '../../../Components/Loader';

const useStyles = makeStyles((theme) => ({
  content: {
    flexGrow: 1,
    width: "90%",
    overflow: 'auto',
    paddingTop: "80px",
    paddingLeft: "5%",
    paddingRight: "5%",
    backgroundColor: "white"
  },
  headerContent: {
    display: "flex",
    width: "100&",
    justifyContent: "space-between",
    marginBottom: "10px"
  },
  mainContent: {
    marginTop: '40px'
  },
  showLinkBtg: {
    textDecoration: 'none'
  },
  inputBox: {
    marginBottom: '20px'
  },
  inputTitle: {
    fontSize: '14px'
  },
  input: {
    height: '30px',
    width: '20%',
    padding: '5px',
    outline: 'none',
    paddingLeft: '7px'
  },
  inputTextarea: {
    padding: '5px',
    paddingLeft: '7px'
  },
  buttonBox: {
    marginBottom: '100px',
    marginTop: '60px'
  },
  saveButton: {
    backgroundColor: "black",
  }
}));

export default function CreateAssumption() {
  const classes = useStyles();
  const cookies = new Cookies();
  const token = cookies.get('auth');
  const [isLoggedIn, setIsLoggedIn] = useState(true);
  const [isCreating, setIsCreating] = useState(false);
  const [name, setName] = useState('');
  const [value, setValue] = useState('');

  const saveAssumption = () => {
    if(!name) {
      swal('Warning', 'Name is empty');
      return;
    }
    if(!value) {
      swal('Warning', 'Value is empty');
      return;
    }
    setIsCreating(true);
    axios({
      method: "POST",
      url: baseUrl + 'assumption/create',
      data: {
        name, 
        value: parseInt(value)
      },
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then((res) => {
      swal('Success', 'Assumption created successfully');
      return;
    })
    .catch((error) => {
      swal('Error', 'Assumption created failed');
      return;
    })
    .finally(() => {
      setIsCreating(false);
      setName('');
      setValue('');
      return;
    })
  }

  useEffect(() => {
    async function getValidate() {
      let isValidated = await validateToken();
      if(!isValidated) {
        setIsLoggedIn(false);
      }   
    }
    getValidate()
  }, [])

  if(!isLoggedIn) return <Redirect to='/login'/>

  return (
    <main className={classes.content}>

      <Grid className={classes.headerContent}>
        <Typography 
          variant="h6" 
          gutterBottom
        >
          Create Assumption 
        </Typography>
        <Link to='/assumption' className={classes.showLinkBtg}>
          <Button
            variant="contained"
            color="primary"
          >
            Show
          </Button>
        </Link>
      </Grid>

      <div>

        <div className={classes.inputBox}> 
          <p className={classes.inputTitle}>Name</p>
          <input 
            onChange={(e) => setName(e.target.value)}
            value={name}
            className={classes.input}
            placeholder="Assumption Name"
            type="text"
          />
        </div>

        <div className={classes.inputBox}>
          <p className={classes.inputTitle}>Value</p>
          <input 
            onChange={(e) => setValue(e.target.value)}
            value={value}
            className={classes.input}
            placeholder="Assumption Value"
            type="number"
          />
        </div>
      </div>

      <div className={classes.buttonBox}>
        <Button
          onClick={saveAssumption}
          className={classes.saveButton}
          variant="contained"
          color="primary"
        >
        {
          isCreating
          ?
          <center>
            <Loader />
          </center>
          :
          <>
            {`Save Assumtion`}
          </>
        }
        </Button>
      </div>

    </main>
  )
}