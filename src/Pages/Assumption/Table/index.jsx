import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {
  Button
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';
import axios from 'axios';
import baseUrl from '../../../Common/apiurl';
import Loader from '../../../Components/Loader';

const useStyles = makeStyles((theme) => ({
  buttonBox: {
    display: "flex",
    justifyContent: "space-between"
  },
  link: {
    textDecoration: 'none'
  }
}));

export default function AssumptionTable() {
  const classes = useStyles();
  const cookies = new Cookies();
  const token = cookies.get('auth');
  const [assumptionData, setAssumptionData] = useState([]);
  const [isGettingData, setIsGettingData] = useState(false);

  const getAssumptionData = () => {
    setIsGettingData(true);
    axios({
      method: "GET",
      url: baseUrl + 'assumption/get/by-creator',
      headers: { 
        Authorization: `Bearer ${token}`
      },
    })
    .then((res) => {
      setAssumptionData(res.data.data);
    })
    .catch((err) => {
      console.log(err)
    })
    .finally(() => {
      setIsGettingData(false);
    })
  }

  useEffect(() => {
    getAssumptionData()
  }, [])

  if(isGettingData) return <Loader />

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Value</TableCell>
            <TableCell align="right">Created At</TableCell>
            <TableCell align="right">Last Update</TableCell>
            <TableCell align="right"></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {assumptionData.map((row) => (
            <TableRow
              key={row.id}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell>{row.name}</TableCell>
              <TableCell 
                component="th" 
                scope="row" 
              >
                {row.value}
              </TableCell>
              <TableCell align="right">{row.createdAt}</TableCell>
              <TableCell align="right">{row.updatedAt}</TableCell>
              <TableCell align="right">
                <div className={classes.buttonBox}>
                  <Link to={`/classes-edit/${row.uuid}`} className={classes.link}>
                    <Button
                      variant="contained"
                      color="primary"
                      size="small"
                    >
                      Edit
                    </Button>
                  </Link>

                  <Link to='/classes-delete' className={classes.link}>
                    <Button
                      variant="contained"
                      color="secondary"
                      size="small"
                    >
                      Delete
                    </Button>
                  </Link>
                </div>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
