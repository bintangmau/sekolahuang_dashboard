import axios from 'axios';
import url from './apiurl';
import Cookies from 'universal-cookie';

export default async function validateToken() {
    try {
        const cookies = new Cookies();
        const token = await cookies.get('auth');
        const data = await axios({
            method: "GET",
            url: `${url}auth/validate-token`,
            headers: {
                Authorization: `Bearer ${token}`
            }
        })

        return data.data.status;
    } catch(error) {
        return false
    }
}